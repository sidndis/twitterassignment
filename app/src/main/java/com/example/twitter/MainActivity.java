package com.example.twitter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.StringTokenizer;

import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;


public class MainActivity extends ActionBarActivity
{
    Twitter twitter;
    MyAdapter ad;
    ListView lv;
    MyHandler md;
    Bitmap bmp[];
    String screenName[];

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        md = new MyHandler();
        ad = new MyAdapter();
        lv = (ListView)findViewById(R.id.listView1);
        bmp = new Bitmap[100];
        screenName = new String[100];
        Global.bitmap = new Bitmap[100];
        new Thread(new CreatConnection()).start();
        Toast.makeText(this, "Loading....", Toast.LENGTH_LONG).show();
        for(int i = 0;i<100;i++)
        {
            bmp[i] = BitmapFactory.decodeResource(getResources(), R.drawable.dummy);
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> av, View v,int position, long id)
            {
                Intent in = new Intent(getApplicationContext(),FullScreen.class);
                in.putExtra("n", position);
                startActivity(in);
            }
        });
    }


    class CreatConnection implements Runnable
    {

        @Override
        public void run()
        {

            twitter = new TwitterFactory().getInstance();
            twitter.setOAuthConsumer("yFJcurzBjG9kO51rdIvjwzEYj", "gzi9ZgI8Cw2uuJb8ZE2Z4DOM4uvyOhS4dCAH9oyFYnQ3VqwWLk");
            twitter.setOAuthAccessToken(new AccessToken("2704008564-PpuGq0PTtClAYNDb638kxL7RUsWdZRSZ06W85Ri", "HOwe9gPqYU6pn2hdkalp0BusMdy0YINNHoFrz8bQc4ZfE"));


            new Thread(new GetUsers()).start();
        }

    }

    class GetUsers implements Runnable
    {


        @Override
        public void run()
        {

            try
            {


                List<Status> statuses = twitter.getUserTimeline("Humblebrag",new Paging(1, 120));
                int i = 0;
                for (Status status : statuses)
                {

                    String s = status.getText();
                    if(s.startsWith("RT"))
                    {
                        StringTokenizer st = new StringTokenizer(s,"@:");
                        st.nextToken();
                        screenName[i] = st.nextToken();

                        i++;
                        if(i == 100)
                        {
                            break;
                        }

                    }


                }


                md.sendEmptyMessage(1);
                new Thread(new FetchPhotos()).start();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }

    }

    class MyAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {

            return screenName.length;
        }

        @Override
        public Object getItem(int i)
        {

            return screenName[i];
        }

        @Override
        public long getItemId(int i)
        {

            return i;
        }

        @Override
        public View getView(int i, View singleUser, ViewGroup parent)
        {
            if(singleUser == null)
            {
                LayoutInflater in = LayoutInflater.from(parent.getContext());
                singleUser = in.inflate(R.layout.single_user, parent, false);
            }

            TextView tv = (TextView)singleUser.findViewById(R.id.singleUser_userName);
            ImageView img = (ImageView)singleUser.findViewById(R.id.image);

            String user = screenName[i];

            tv.setText(user);
            img.setImageBitmap(bmp[i]);

            return singleUser;
        }

    }

    class MyHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg)
        {

            findViewById(R.id.progressBar1).setVisibility(View.GONE);
            lv.setAdapter(ad);

        }
    }

    public class FetchPhotos implements Runnable
    {
        int i = 0;
        public void run()
        {



            for(int i = 0;i<100;i++)
            {
                new Thread(new SinglePhoto(i)).start();

            }


        }

    }

    class SinglePhoto implements Runnable
    {
        int i;
        public SinglePhoto(int a)
        {
            i = a;
        }
        @Override
        public void run()
        {
            try
            {

                BufferedInputStream bis = null;
                InputStream is = null;
                URL aURL = new URL(twitter.showUser(screenName[i]).getOriginalProfileImageURL());
                URLConnection conn = aURL.openConnection();
                conn.connect();
                is = conn.getInputStream();
                bis = new BufferedInputStream(is);


                if(bis!=null)
                {
                    Global.bitmap[i] = bmp[i] = BitmapFactory.decodeStream(bis);

                    bis.close();
                    is.close();
                }
                runOnUiThread(new Runnable()
                {

                    @Override
                    public void run()
                    {
                        ad.notifyDataSetChanged();

                    }
                });
            }
            catch(Exception e)
            {

                e.printStackTrace();
            }


        }

    }


}

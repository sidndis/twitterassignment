package com.example.twitter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.support.v4.view.*;

public class FullScreen extends ActionBarActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen);
        ExtendedViewPager mViewPager = (ExtendedViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new TouchImageAdapter());
        int n = getIntent().getIntExtra("n", 0);
        mViewPager.setCurrentItem(n,false);
    }

    class TouchImageAdapter extends PagerAdapter
    {



        @Override
        public int getCount()
        {
            return Global.bitmap.length;
        }


        public View instantiateItem(ViewGroup container, int position)
        {
            TouchImageView img = new TouchImageView(container.getContext());
            img.setImageBitmap(Global.bitmap[position]);
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object)
        {
            return view == object;
        }

    }


}
